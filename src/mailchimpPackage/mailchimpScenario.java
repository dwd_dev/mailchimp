package mailchimpPackage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.List;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.*;
import com.opencsv.CSVWriter;

	public class mailchimpScenario {
	    public String url = "https://mailchimp.com/";
	    String driverPath = System.getProperty("user.dir")+"/drivers/chromedriver";
	    public WebDriver driver ; 
	    public String timeStamp = new SimpleDateFormat("MM-dd-yyyy-hh_mm_ss_a").format(new java.util.Date());

	  @Test (priority=1)
	  public void navigateToAboutPage() {
		  System.out.println("launching chrome browser"); 
	      System.setProperty("webdriver.chrome.driver", driverPath);
	      driver = new ChromeDriver();
	      driver.get(url);
	      
		  System.out.println("navigating to About page"); 
	      driver.get(url + "about/");

	  }
	  
	  @Test (priority=2)
	  public void getTeamMembers() throws IOException {
		  System.out.println("getting members");
		  
		  	//Names
		    List<WebElement> name = driver.findElements(By.xpath("//*[@id=\'page-about']/body/div[2]/article/div/section[5]/div/div/a/h3")); 
		    String []names =new String[name.size()];
		    
		    //Positions
		    List<WebElement> position = driver.findElements(By.xpath("//*[@id=\'page-about']/body/div[2]/article/div/section[5]/div/div/a/span")); 
		    String []positions =new String[position.size()];
		    
		    //Bios
		    List<WebElement> bio = driver.findElements(By.xpath("//*[@id=\'page-about']/body/div[2]/article/div/section[5]/div/div/a")); 
		    String []bios =new String[bio.size()];

		    //Create .csv file
		    String fileName = System.getProperty("user.dir")+"/csv_output/members" + timeStamp + ".csv";

		    //Store elements in array
		    int n=0;
		    for(WebElement eachName: name)
		    {
		    	names[n]=eachName.getText();
		    		
		       n++;
		       
		    }
		    
		    int p=0;
		    for(WebElement eachPosition: position)
		    {
		    	positions[p]=eachPosition.getText();
		    		
		       p++;
		       
		    }
		    
		    int b=0;
		    for(WebElement eachBio: bio)
		    {
		    	bios[b]=eachBio.getAttribute("data-description");
		    		
		       b++;
		       
		    }
		        try (FileOutputStream fos = new FileOutputStream(fileName);
		                OutputStreamWriter osw = new OutputStreamWriter(fos, 
		                        StandardCharsets.UTF_8);
		                CSVWriter writer = new CSVWriter(osw)) {
		            
		            writer.writeNext(names);
		            writer.writeNext(positions);
		            writer.writeNext(bios);
		        }
 
		    
	  }
	  
	  @Test (priority=3)
	  public void exitBrowser() {
		  driver.quit();
	  }
	 
	  
}

	  
	
